#!/bin/bash 

# sjekk om jo er installert:

if ! which jo > /dev/null; then
	echo "You need to have jo installed"
	apt-get install jo -y
fi

# hvilken URL som skal brukes. tilpass dette:
URL="http://admin:iaml0rdv0ldem0rt@10.212.140.89:5984/_design/clog"

# lag JSON
JSON=$( jo message="$1" date="$(date)" host=$HOSTNAME )

# send til databasen
curl -H "Content-Type: application/json" -X POST -d "$JSON" $URL
