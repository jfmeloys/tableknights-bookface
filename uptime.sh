#!/bin/bash -x

server1=192.168.128.79
server2=192.168.132.233
server3=192.168.132.101
docker=192.168.128.123
db1=192.168.131.0
www1=192.168.128.151
www2=192.168.128.80
balancer=192.168.128.148


hosts=($server1 $server2 $server3)


source /home/ubuntu/DCSG2003_V21_group3-openrc.sh

for i in "${hosts[@]}" 
do
	:
# do whatever on $i done
ssh ubuntu@$i "uptime"
if [ $? -eq 0 ]
then
	echo "$i is running"	
else	
	echo "$i is not running, try reboot"	      
	openstackid=$(openstack server list | grep $i | awk '{ print $2}')
	echo $openstackid
	openstack server start $openstackid
fi
done 

# Denne løsningen er mye bedre men fungerer ikke hvis man skal filtrere ut enkelt vm-er slik som vi gjør med docker-vmen.

# for server in $(openstack server list | grep SHUTDOWN | awk '{ print $2 }'); do
# openstack server start $server
# done
