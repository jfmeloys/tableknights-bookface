#!/bin/bash -ex

source /home/ubuntu/DCSG2003_V21_group3-openrc.sh

VM_IP=$( openstack server list | grep -w "docker" | awk '{ print $8 }' | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}")

echo $VM_IP

scp startmemcache.sh ubuntu@$VM_IP:/home/ubuntu

ssh ubuntu@$VM_IP "./startmemcache.sh"
