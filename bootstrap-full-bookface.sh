#!/bin/bash -x
#############
#
#
# Full Bookfacece deployment
# Uses Docker swarm, CockroachDB as a cluster, GLusterFS storage for images
# Memcached, HAproxy, consul and automated scaling.
#
#############

#### Important variables. You have to set these:

# The name of the company.
COMPANY_NAME="TA2"

# Paste your openstack variables here:
export OS_AUTH_URL=""
export OS_PROJECT_ID=""
export OS_PROJECT_NAME=""
export OS_USER_DOMAIN_NAME=""
export OS_PROJECT_DOMAIN_ID=""
export OS_USERNAME=""
export OS_PASSWORD=""
export OS_REGION_NAME=""
export OS_INTERFACE=public
export OS_IDENTITY_API_VERSION=3

# Set the migration key to use. This key enables you to import data into this system
# from a previous installation
MIGRATION_KEY=""

# Import data from another bookface installation. 
# If you leave this blank, the site will be set up but no data will be imported:
MIGRATION_SOURCE=""

# Import data from this URL instead. This is in order to get a fully working setup with content
# without having an existing system. This should not be combined with MIGRATION SOURCE.
IMPORT_DATA="http://192.168.128.5:9001/bf_21k.tgz"


# Discord webhook URL to a text channel.
# Use this to get messages of the deployment state.
DISCORD_URL=""

# Swarmprom will be downloaded and ready to be used.
# Should the swarmprom grafana/prometheus stack be started? Change to "1"
# If you use a small flavor, perhaps leave this off
ENABLE_SWARMPROM=""

# Should Consul be installed and set up?
# If not, leave blank
ENABLE_CONSUL="1"

# Should the cluster start automatic scaling of frontend users based on download times?
# The scaling will only start once data import is complete. It can also be enabled later.
# This assumes that there is a company name and that the "uptime challenge" system is
# monitoring the the site.
ENABLE_AUTOSCALING="1"

# Too many frontpage users can crash the site, what is the upper limit to the autoscaling?
MAX_FRONTPAGE_USERS=3000


###############################################################################################
#  You don't have to change anything else fom this point
###############################################################################################

# We will guess the name of the servers, based on the hostname
SERVER_PREFIX=$( echo $HOSTNAME | sed 's/-.*//g')

# What version of cockroachDB to use
COCKROACHDB_VERSION="v20.2.4"

# Other OpenStack variables that need to be there
export OS_AUTH_URL=https://api.skyhigh.iik.ntnu.no:5000
export OS_INTERFACE=public
export OS_IDENTITY_API_VERSION=3

#### Other stuff, like SQL code and config files
COCKROACHDB_CREATE_DB="
CREATE DATABASE bf;
CREATE USER bfuser;
GRANT ALL ON DATABASE bf TO bfuser;

USE bf;

CREATE TABLE users (
userid INT NOT NULL DEFAULT unique_rowid(),
name STRING(50) NULL,
picture STRING(300) NULL,
status STRING(10) NULL,
posts INT NULL,
comments INT NULL,
lastpostdate TIMESTAMP NULL DEFAULT now():::TIMESTAMP,
createdate TIMESTAMP NULL DEFAULT now():::TIMESTAMP,
FAMILY \"primary\" (userid, posts, comments, lastpostdate),
FAMILY \"secondary\" (name, picture,status, createdate)
);


CREATE TABLE posts (
postid INT NOT NULL DEFAULT unique_rowid(),
userid INT NOT NULL,
text STRING(300) NULL,
name STRING(150) NULL,
postdate TIMESTAMP NULL DEFAULT now():::TIMESTAMP,
INDEX posts_auto_index_posts_users_fk (userid ASC),
FAMILY \"primary\" (postid, userid, text, name, postdate)
);
    

CREATE TABLE comments (
commentid INT NOT NULL DEFAULT unique_rowid(),
userid INT NOT NULL,
postid INT NOT NULL,
text STRING(300) NULL,
postdate TIMESTAMP NULL DEFAULT now():::TIMESTAMP,
INDEX comments_userid_idx (userid ASC),
INDEX comments_postid_idx (postid ASC),
FAMILY \"primary\" (commentid, userid, postid, text, postdate)
);

CREATE table config ( key STRING(100), value STRING(500) );
insert into config ( key, value ) values ( 'migration_key', '$MIGRATION_KEY' );

create index on users (name);
create index on users (userid);
create index on users (lastpostdate);
create index on posts (userid);
create index on posts (postdate);
"

# This is the content of the haproxy file we will be using
HAPROXY_START="
global
    log         127.0.0.1 local2
    stats socket /run/admin.sock mode 660 level admin expose-fd listeners
    stats timeout 30s
    pidfile     /var/run/haproxy.pid
    maxconn     4000

defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000

listen stats
    bind *:1936
    stats enable
    stats uri /
    stats hide-version
    stats auth someuser:password


cache bfcache
      total-max-size 128
      max-age 6000

frontend bookface
        bind *:80
	mode http
	default_backend docker_bf_web_service
	acl use_cache path -i -m beg /images
	use_backend cachedcontent if use_cache
	
backend cachedcontent
        balance roundrobin
	http-request cache-use bfcache
	http-response cache-store bfcache
	server web web:80
	
backend docker_bf_web_service
        balance roundrobin
	server web web:80

frontend  db
    bind *:26257
    mode tcp
    default_backend databases

backend databases
    mode tcp
    balance     roundrobin
"

# /etc/systemd/system/bookface.service
# For automatic starting of everything in the right order after booting
SYSTEMD_BOOKFACE_SERVICE="
[Unit]
Description=Bookface script

[Service]
Type=simple
KillMode=process
ExecStart=/root/bookface_start.sh

[Install]
WantedBy=multi-user.target
"

# The actual startup script which starts everything
# It waits for glusterfs to be able to mount before doing anything else.
SYSTEMD_BOOKFACE_STARTUP="#!/bin/bash

log_message 'What happened? Did someone shut me down? Time to start everything up again.'

ntpdate -b ntp.justervervesenet.no

log_message 'Mounting GlusterFS folders'

mount -t glusterfs HOSTNAME:/bf_config /bf_config
sleep 3
while [ ! -f /bf_config/join-token ]; do

echo \"Could not find joint-token file, assuming gluster mount did not succeed, trying again in 5 seconds\"
sleep 10
umount /bf_config
mount -t glusterfs HOSTNAME:/bf_config /bf_config
sleep 2
done

mount -t glusterfs HOSTNAME:/bf_images /bf_images
sleep 3
while [ ! -f /bf_images/im_mounted ]; do
sleep 10
umount /bf_images
mount -t glusterfs HOSTNAME:/bf_images /bf_images
sleep 2
done

sleep 2

log_message 'GlusterFS mountet. Starting Docker'
systemctl start docker

"

# The Docker compose file for the bookface stack.
# The main difference is that it is HAproxy which also 
# listens om port 80 because it does caching of images

DOCKER_COMPOSE_FILE='
version: "3.3"

configs:
  bf_config:
    file: ./config.php

  haproxy_config:
    file: ./haproxy.cfg

networks:
  bf:
    attachable: true

services:
  web:
    image: docker.cs.hioa.no/kyrrepublic/bf:latest
    
    restart: always

    configs:
      - source: bf_config
        target: /var/www/html/config.php

    networks:
        - bf
    volumes:
        - type: bind
          source: /bf_images
          target: /var/www/html/images

    environment:
      BF_MEMCACHE_SERVER: memcache
      BF_DB_HOST: balancer
      BF_DB_PORT: 26257
      BF_DB_USER: bfuser
      BF_DB_NAME: bf
      BF_FRONTPAGE_LIMIT: 500
      
    deploy:
      replicas: 3

  memcache:
    image: memcached
    restart: always
    
    networks:
      - bf

  balancer:
    image:  kyberna/haproxy
    
    configs:
      - source: haproxy_config
        target: /usr/local/etc/haproxy/haproxy.cfg

    networks:
      - bf
      
    deploy:
      replicas: 3

    ports:
      - "1936:1936"
      - "80:80"
      '

# A script to start cockroachdb should it not be running. Will be executed by cron every 5 minutes.
# This is in case cockroachdb crashes. It may not fix all problems.
# For instance, if clock-synchronization is the issue, then this script
# will simply attempt to start it every five minutes without success.

CHECK_COCKROACHDB='#!/bin/bash
if ! pgrep -x cockroach >/dev/null; then

if [ -f /bf_images/im_mounted ]; then

log_message "Ooops. CockroachDB seems not to run. Trying to start it."

CDBCOMMAND

fi
fi
'


# This script sends messages to a Discord text channel
# It uses the built-in variable $PPID to get the name of the parent process so that we know who used the command
DISCORD_LOG='#!/bin/bash

message="$1"
MYIP=$(ip addr show ens3 | grep "inet " | sed -e "s/.* inet //g" | sed -e "s/\/.*//g")
PARENT=$(tr -d "\0" </proc/$PPID/cmdline)

URL="DISCORD_URL"

USERNAME="Bookface [$HOSTNAME / $MYIP] $PARENT"

if echo $USERNAME | grep "/var/lib/cloud/"; then 
USERNAME="Bookface [$HOSTNAME / $MYIP] cloud-init"
fi

USERNAME=$( echo $USERNAME | sed -e "s/\/bin\/bash//" )

JSON="{\"username\": \"$USERNAME\", \"content\": \"$message\"}"

curl -s -X POST -H "Content-Type: application/json" -d "$JSON" $URL
'

# This script is used instead of the one above in case the variable DISCORD_URL is empty
# the message is sent to /var/log/syslog instead
DISCORD_LOG_DUMMY='#!/bin/bash

# Send message to syslog
logger "$1"
'

# This script is used to automatically adjust the number of users on the frontpage based on the last observed download times.
# It also incorporates a MAX value ( optional ) and a danger zone, where scaling will be done in smaller increments. This 
# allows for rapid scaling early, but careful scaling once a certain threshold has been reached.
# This script is installed on all three servers, but only the one who is currently the leader
# will actually do the scaling.

FRONTPAGE_SCALE='#!/bin/bash

# Only run if you are the swarm leader
if docker node ls | grep $HOSTNAME | grep Leader >/dev/null; then echo "I am the leader, I will act"; else exit 0; fi

# Name of company
COMPANY_NAME="COMPANY_NAME_PLACEHOLDER"

# What is the lowest desired number of frontpage users?
FRONTPAGE_COUNT_LIMIT=500

# What increments do we increase in?
SCALE_UP_INCREMENT=200

# What increments do we decrease in?
SCALE_DOWN_INCREMENT=250

# What is the lower download time limit we would like to stay above?
DOWNLOAD_TIME_LOWER_THRESHOLD=4.5

# What is the upper download time limit we would like to stay below?
DOWNLOAD_TIME_UPPER_THRESHOLD=5.5

# SAFETY VALVE: Set this to "0" if you want the scaling to actually take place
SAFETY_VALVE=0

# DANGER ZONE. After this amount of users, we will divide the increments divided by 10.
DANGER_ZONE=1800

########################
# We will only run if there is a company name different from "COMPANY_NAME_PLACEHOLDER"
# Name of company
COMPANY_NAME=$(docker service inspect bf_web | grep COMPANY_NAME | tail -1 | sed -e "s/.*=//" | sed -e "s/\".*//")
if [ -z "$COMPANY_NAME" ]; then
echo "Scaling is disabled. Execute the command: docker service update --env-add COMPANY_NAME=\"YOUR_COMPANY_NAME\" bf_web"
exit 0;
fi

########################
# SAFETY CHECK: Is bc installed?
if ! which bc > /dev/null; then
echo "You need to install the package bc first"
exit 1
fi

########################
# Define function for scaling

function scale {
COMMAND="docker service update --env-add BF_FRONTPAGE_LIMIT=$1 bf_web"
$COMMAND
}

################################
# Check if the site is up. Exit if its down.

STATUS=$( curl -s -g "http://admin:admin@192.168.128.5:9090/api/v1/query?query=last_status{name=\"$COMPANY_NAME\"}" | jq -r ".data.result[].value[1] ")

if [ "$STATUS" -gt "0" ]; then
echo "Site is considered up"
else
echo "Site is considered down, we should stop here"
exit 1
fi

################################
# The site is up, so we can proceed with checking its performance

# Get current download times:

DOWNLOAD_TIME=$( curl -s -g "http://admin:admin@192.168.128.5:9090/api/v1/query?query=last_download_time{name=\"$COMPANY_NAME\"}" | jq -r ".data.result[].value[1] ")

# What was the observed number of frontpage users?

NUMBER_OF_FRONTPAGE_USERS=$( curl -s -g "http://admin:admin@192.168.128.5:9090/api/v1/query?query=frontpage_count{name=\"$COMPANY_NAME\"}" | jq -r ".data.result[].value[1] ")

# do we have a max numbers?

MAX_USERS=$(docker service inspect bf_web | grep BF_MAX_FRONTPAGE_USERS | tail -1 | sed -e "s/.*=//" | sed -e "s/\".*//")

echo "Observed download time: $DOWNLOAD_TIME"

# check if we are below the lower threshold. If we are, we scale up
if (( $(echo "$DOWNLOAD_TIME < $DOWNLOAD_TIME_LOWER_THRESHOLD" | bc -l) )); then

if [ -n "$MAX_USERS" ]; then
if (( $(echo "$NUMBER_OF_FRONTPAGE_USERS >= $MAX_USERS" | bc -l) )); then
exit 0
fi
fi


if (( $(echo "$NUMBER_OF_FRONTPAGE_USERS > $DANGER_ZONE" | bc -l) )); then

NEW_FRONTPAGE_COUNT=$( echo "$NUMBER_OF_FRONTPAGE_USERS + ($SCALE_UP_INCREMENT/10)" | bc )

log_message "Download time: $DOWNLOAD_TIME. We have some capacity to spare. Scaling only up to $NEW_FRONTPAGE_COUNT, because we are in the danger zone (above $DANGER_ZONE)."

else

NEW_FRONTPAGE_COUNT=$( echo "$NUMBER_OF_FRONTPAGE_USERS + $SCALE_UP_INCREMENT" | bc )

log_message "Download time: $DOWNLOAD_TIME. We have some capacity to spare. Scaling up to $NEW_FRONTPAGE_COUNT"

fi

if [ -n "$MAX_USERS" ]; then
if (( $(echo "$NEW_FRONTPAGE_COUNT > $MAX_USERS" | bc -l) )); then
log_message "Adjusting the number to the maximum number $MAX_USERS"
NEW_FRONTPAGE_COUNT=$MAX_USERS
fi
fi


scale $NEW_FRONTPAGE_COUNT

# check if we are above the higher threshold. If we are, scale down, but not lower than the limit
elif (( $(echo "$DOWNLOAD_TIME > $DOWNLOAD_TIME_UPPER_THRESHOLD" | bc -l) )); then

# We cannot go lower than the bottom
if [ "$NUMBER_OF_FRONTPAGE_USERS" -eq "$FRONTPAGE_COUNT_LIMIT" ]; then

echo "We should go lower, but we are already at the limit"

exit 0

fi

# Lowering the number of frontpage users
NEW_FRONTPAGE_COUNT=$( echo "$NUMBER_OF_FRONTPAGE_USERS - $SCALE_DOWN_INCREMENT" | bc )

if [ "$NEW_FRONTPAGE_COUNT" -lt "$FRONTPAGE_COUNT_LIMIT" ]; then

echo "We should scale down, but cannot go lower then the limit, so we end up at $FRONTPAGE_COUNT_LIMIT"

NEW_FRONTPAGE_COUNT=$FRONTPAGE_COUNT_LIMIT

else

log_message "Download time $DOWNLOAD_TIME. Scaling down to $NEW_FRONTPAGE_COUNT."
scale $NEW_FRONTPAGE_COUNT

fi
fi
'

############################################################################################
############################################################################################
## Time to install something.
## This part is execute by all the servers.
###########################################################################################
############################################################################################

####
# Install log_message command do it can be used throughout
####

if [ -n "$DISCORD_URL" ]; then
# we need to use a different character as the regex delimiter in sed, since the URL itself contains a /, which would confuse sed
echo "$DISCORD_LOG" | sed -e "s@DISCORD_URL@$DISCORD_URL@g" > /usr/bin/log_message
else
echo "$DISCORD_LOG_DUMMY" > /usr/bin/log_message
fi

chmod +x /usr/bin/log_message

# Time to say hello 

log_message "Reporting for duty!"

####
# Make root login possible
####

cp /home/ubuntu/.ssh/authorized_keys /root/.ssh/


####
# Install frontpage_scale script
####

log_message "Installing autoscaling script /root/frontpage_scale.sh. Disabled by default. It will run every 5 minutes once it is enabled."
echo "$FRONTPAGE_SCALE" | sed -e "s/COMPANY_NAME_PLACEHOLDER/$COMPANY_NAME/g" > /root/frontpage_scale.sh
chmod +x /root/frontpage_scale.sh
echo "*/5 * * * * root /root/frontpage_scale.sh" >> /etc/crontab

####
# Keep clocks synchronized
####

log_message "Adding NTP synchronization to /etc/crontab"
echo "*/5 * * * * root ntpdate -b ntp.justervesenet.no" >> /etc/crontab


####
# The "RC File" with openstack credentials is /root/.openstack
# Add sourcing of this file to .bashrc, so one can execute openstack commands effortlessly
####

echo "export OS_AUTH_URL=$OS_AUTH_URL" >> /root/.openstack
echo "export OS_PROJECT_ID=$OS_PROJECT_ID" >> /root/.openstack
echo "export OS_PROJECT_NAME=$OS_PROJECT_NAME" >> /root/.openstack
echo "export OS_USER_DOMAIN_NAME=$OS_USER_DOMAIN_NAME" >> /root/.openstack
echo "export OS_PROJECT_DOMAIN_ID=$OS_PROJECT_DOMAIN_ID" >> /root/.openstack
echo "export OS_USERNAME=$OS_USERNAME" >> /root/.openstack
echo "export OS_PASSWORD=$OS_PASSWORD" >> /root/.openstack
echo "export OS_REGION_NAME=$OS_REGION_NAME" >> /root/.openstack
echo "export OS_INTERFACE=$OS_INTERFACE" >> /root/.openstack
echo "export OS_IDENTITY_API_VERSION=$OS_IDENTITY_API_VERSION" >> /root/.openstack

echo "source /root/.openstack" >> /root/.bashrc


###
# Install Docker
###

log_message "Installing Docker and other packages"

apt-get update

apt-get install -y apt-transport-https ca-certificates curl software-properties-common python3-openstackclient python3-octaviaclient ntpdate jq bc wget2

ntpdate -b ntp.justervesenet.no

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt-get update

apt-get install -y docker-ce git

echo '{
     "metrics-addr" : "0.0.0.0:9323",
     "experimental": true
}' > /etc/docker/daemon.json

service docker restart

# disable docker from starting at boot
systemctl disable docker
systemctl disable docker.service
systemctl disable docker.socket



###
# Install CockroachDB
###

log_message "Installing cockroachdb"

wget https://binaries.cockroachdb.com/cockroach-${COCKROACHDB_VERSION}.linux-amd64.tgz
tar xzf cockroach-${COCKROACHDB_VERSION}.linux-amd64.tgz
cp cockroach-${COCKROACHDB_VERSION}.linux-amd64/cockroach /usr/local/bin
mkdir /bfdata


###
# Install GLusterFS
###

log_message "Installing GlusterFS"

add-apt-repository -y ppa:gluster/glusterfs-8
apt-get update
apt-get -y install glusterfs-server glusterfs-client
systemctl enable glusterd
systemctl start glusterd

mkdir /bf_brick
mkdir /config_brick
mkdir /bf_images
mkdir /bf_config

###
# Set up proper hostnames
###
openstack server list -c Networks -c Name -f csv | grep $SERVER_PREFIX | sed -e 's/,".*=/,/' | sed -e 's/"//g' | sed -e 's/,/ /' | awk '{ print $2 " " $1 }' >> /etc/hosts

# create the unique join string with all the servers:
CDB_JOIN_STRING=${SERVER_PREFIX}-1":26257"
for server in $( cat /etc/hosts | grep $SERVER_PREFIX | grep -v ${SERVER_PREFIX}-1 | awk '{print $2}' ); do
CDB_JOIN_STRING="$CDB_JOIN_STRING,$server:26257"
done

#####
# Set up the start script
#####

log_message "Installing /root/bookface_start.sh for when the server boots"
echo "$SYSTEMD_BOOKFACE_SERVICE" > /etc/systemd/system/bookface.service

echo "$SYSTEMD_BOOKFACE_STARTUP" | sed -e "s/HOSTNAME/$HOSTNAME/g" > /root/bookface_start.sh

chmod +x /root/bookface_start.sh

systemctl enable bookface


####
# Create script which starts shutdown servers
####

echo "#!/bin/bash" > /root/start_servers.sh

# remember to source the file with all the variables.
echo "source /root/.openstack" >> /root/start_servers.sh
# add a start command for each of the servers. 
# Nothing happens if we try to start a server which is already running, so no need to check if is down or not.
for server in $( cat /etc/hosts | grep $SERVER_PREFIX | awk '{print $2}' ); do
echo "if openstack server start $server 2>>/dev/null; then log_message '$server was just started by me'; fi" >> /root/start_servers.sh
done

# It should run every five minutes
chmod +x /root/start_servers.sh
echo "*/5 * * * * root /root/start_servers.sh" >> /etc/crontab


########################################################################################################
########################################################################################################
## The next part is divided by an IF-test. If the hostname ends in "-1", like server-1, then this
## server will be a "manager" and initiate the database,glusterfs and docker clusters.
## However, once the setup is completed, all three servers will have manager privileges
## and be able to function as managers.
########################################################################################################
########################################################################################################

###
# Check If you are the manager
###

if hostname | egrep '[a-z]-1$'; then
## This is the first swarm node, and therefore the manager

###
# Start to download data from the other site in the background if we are going to migrate data.
# We are mainly interested in the images, which we will copy directly into the /bf_images folder later
###
if [ ! -z "$MIGRATION_SOURCE" ]; then
log_message "Starting to download data from $MIGRATION_SOURCE in the background. This will take a while."
wget2 -t 3 -T 15 -p --max-threads=20 -q "http://$MIGRATION_SOURCE/showallusers.php" &
fi


#### 
# CockroachDB
# Time to start the database
####

cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=$CDB_JOIN_STRING --advertise-addr=$(hostname):26257 --max-offset=1500ms

# The next commands will add the cockroachdb command to the startup script
CDBCOMMAND="cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=$CDB_JOIN_STRING --advertise-addr=$(hostname):26257 --max-offset=1500ms"
echo "$CDBCOMMAND" >> /root/bookface_start.sh

# These commands will create a special check script, thich monitors if cockroachdb is still running.
# It will help if the database crashes, but not fix all cases. Run every 5 minutes through cron
echo "$CHECK_COCKROACHDB" | sed -e "s@CDBCOMMAND@$CDBCOMMAND@" > /root/check_cockroachdb.sh
chmod +x /root/check_cockroachdb.sh
echo "*/5 * * * * root /root/check_cockroachdb.sh" >> /etc/crontab

#### Get list of worker servers
# Get the IPs of the other nodes

WORKER_SERVERS=$( cat /etc/hosts | grep $SERVER_PREFIX | grep -v $(hostname) | awk '{ print $2 }' )
WORKER_COUNT=$( cat /etc/hosts | grep $SERVER_PREFIX | grep -v $(hostname) | wc -l )

###################################
#### GlusterFS
###################################
VOLUME_IMAGE_PATH="${SERVER_PREFIX}-1:/bf_brick"
VOLUME_CONFIG_PATH="${SERVER_PREFIX}-1:/config_brick"

log_message "Starting process to add the two other servers to the GlusterFS cluster"
# add the other nodes
for server in $WORKER_SERVERS; do

while ! gluster peer probe $server; do
echo "could not peer $server, sleeping"
sleep 5
done
VOLUME_IMAGE_PATH="$VOLUME_IMAGE_PATH $server:/bf_brick"
VOLUME_CONFIG_PATH="$VOLUME_CONFIG_PATH $server:/config_brick"
done

sleep 5
# create the volume for bookface images
log_message "Creating GlusterFS volumes /bf_images and /bf_config"

while ! gluster volume create bf_images replica 3 $VOLUME_IMAGE_PATH force; do
echo "could not create gluster volume, waiting";
sleep 2
done

# create the volume for configs
while ! gluster volume create bf_config replica 3 $VOLUME_CONFIG_PATH force; do
echo "could not create gluster volume, waiting";
sleep 2
done

gluster volume set bf_images auth.allow "*"
gluster volume set bf_config auth.allow "*"


# This brutal shutdown is needed in order to avoid the "authentication denied" behavior
# observed in some cases
gluster volume start bf_images
gluster volume start bf_config

log_message "Mounting GlusterFS volumes"

# mount the volume for bookface images
mount -t glusterfs ${SERVER_PREFIX}-1:bf_images /bf_images
sleep 2
touch /bf_images/im_mounted

# mount the volume for configs
mount -t glusterfs ${SERVER_PREFIX}-1:bf_config /bf_config

sleep 2

# Check if the config folder really is mounted:
if ! df | grep bf_config; then
echo "Could not mount Gluster volume, exiting"
exit 1
fi


### Bookface ( part 1 )
# pull the bookface repository

log_message "Cloning bookface repository and generating special haproxy file with IP addresses of the servers"

git clone https://git.cs.oslomet.no/kyrre.begnum/bookface.git
cd bookface

# We install our own docker compose file: 
echo "$DOCKER_COMPOSE_FILE" > docker-compose.yml

###################################
#### HAproxy
###################################

# Create the config file for HAproxy as
# a frontend for CockroachDB and the webserevers


echo "$HAPROXY_START" > haproxy.cfg

# add the three servers to the end of the file as part of the backend
for server in $( cat /etc/hosts | grep $SERVER_PREFIX | awk '{ print $2 }' ); do
ip=$(cat /etc/hosts | grep $server | cut -f 1 -d " ")
echo "    server   $server  ${ip}:26257" >> haproxy.cfg
done

###################################
#### Docker swarm
###################################

# Initialize the swarm
log_message "Time to set up the Docker Swarm"
docker swarm init

# store the token in /bf_config. The other two servers will look for it and use it to join the swarm
docker swarm join-token worker | grep join | sed -e 's/.*token //g' | sed -e 's/ .*//g' > /bf_config/join-token

log_message "Waiting for the other two servers to mount the glusterFS volume, find the join token and join the swarm"

# wait for the others to join
ACTIVE_WORKERS=$( docker node ls | grep -v AVAILABILITY | grep -v Leader | wc -l )

while ! [ "$WORKER_COUNT" -eq "$ACTIVE_WORKERS" ]; do
  echo "Not all workers are active, waiting"
  sleep 3
  ACTIVE_WORKERS=$( docker node ls | grep -v AVAILABILITY | grep -v Leader | wc -l )
done

log_message "All nodes have joined the swarm. Promoting them to managers, because everyone's a winner!"

# make everyone a leader. This is more robust, should the first server reboot
for server in $(docker node ls | grep -v HOSTNAME | grep -v $HOSTNAME | awk -e '{print $2}'); do
docker node promote $server
done

log_message "Initiating the database and setting up tables"
# initialize the database
cockroach init --insecure --host=$(hostname):26257

# create the tables
echo $COCKROACHDB_CREATE_DB | cockroach sql --insecure --host=localhost:26257

########
# Bookface ( part 2 )
########

log_message "Deploying the Bookface stack"

# start the stack
docker stack deploy -c docker-compose.yml bf

########
# Monitoring with grafana / prometheus
########

# Set up swarmprom for prometheus and grafana for metrics
cd ..
git clone https://github.com/stefanprodan/swarmprom.git

if [ -n "$ENABLE_SWARMPROM" ]; then
log_message "Deploying the prometheus/grafana stack ( available on port 3000 )"
cd swarmprom
# start the stack if it was enabled
docker stack deploy -c docker-compose.yml mon
else
log_message "We will not start swarmprom, even though it is installed"
log_message "To start, type: cd /swarmprom ; docker stack deploy -c docker-compose.yml mon"
fi

#######
# Consul
#######

if [ -n "$ENABLE_CONSUL" ]; then

log_message "Deploying a consul cluster as a data center"

# get the local IP
ip=$(cat /etc/hosts | grep $HOSTNAME | cut -f 1 -d " ")

# remove any dashes from the hostname
DNShostname=$(echo $HOSTNAME | sed -e 's/-//g' )

mkdir /opt/consul
mkdir /opt/consul.conf

# start the container
docker run --restart=always -d -v /opt/consul:/consul/data -v /opt/consul.config:/consul/config --name=consul --net=host -e 'CONSUL_LOCAL_CONFIG={"skip_leave_on_interrupt": true}' -e 'CONSUL_BIND_INTERFACE=ens3'  consul agent -server -bind=$ip --datacenter=$COMPANY_NAME -client=0.0.0.0 -ui -bootstrap -node=$DNShostname

# give it some time to set itself up
sleep 10

# Connect it to the central cluster, which acts as a DNS server
docker run consul join -http-addr=$ip:8500 -wan 192.168.128.5 $ip

# Start the consul_notifier in case we run other docker services that should automatically be added
docker run --restart=always -d --name=consul_notifier -e CONSUL_ADDR=$ip --net=host --volume=/var/run/docker.sock:/var/run/docker.sock docker.cs.hioa.no/kyrrepublic/swarm-consul-notifier:latest

# add the specil environment variables to the 
docker service update --env-add CONSUL_SERVICE_PORT=80 --env-add CONSUL_SERVICE_NAME=bookface bf_balancer

fi

###
# Import data from URL
###

if [ -n "$IMPORT_DATA" ]; then

log_message "We will download and import data directly into the database from this url: $IMPORT_DATA"
# download the tarball:
wget $IMPORT_DATA

# get the name of the tarball:
importfile=$( echo $IMPORT_DATA | sed -e "s/.*\///g" | sed -e "s/.tgz//g" )

tar xzf ${importfile}.tgz

log_message "Running SQL code, be patient."

cockroach sql --insecure < $importfile/${importfile}.sql

log_message "Copying images, give me a moment."

cp $importfile/images/* /bf_images

fi

###################################
#### OpenStack Load Balancer
###################################

log_message "Time to set up the loadbalancer in openstack"

# Create the loadbalancer
openstack loadbalancer create --name bflb-$SERVER_PREFIX --vip-subnet-id  c3ea9f88-8381-46b0-80e0-910c676a0fbd

# wait for it to become ready
while ! openstack loadbalancer show bflb-$SERVER_PREFIX | grep ACTIVE; do
sleep 2
done

# Create the listener
openstack loadbalancer listener create --name bflistener-$SERVER_PREFIX --protocol HTTP --protocol-port 80 bflb-$SERVER_PREFIX

# wait for it to become ready
while ! openstack loadbalancer listener show bflistener-$SERVER_PREFIX | grep ACTIVE; do
sleep 2
done

# Create the pool
openstack loadbalancer pool create --name bfpool-$SERVER_PREFIX --lb-algorithm ROUND_ROBIN --listener bflistener-$SERVER_PREFIX --protocol HTTP

# Add all three servers to the pool
for server in $( cat /etc/hosts | grep $SERVER_PREFIX | awk '{ print $2 }' ); do
ip=$(cat /etc/hosts | grep $server | cut -f 1 -d " ")
openstack loadbalancer member create --address $ip --protocol-port 80 bfpool-$SERVER_PREFIX
done

# create a new floating IP, and store the ID in the variable floating_ip_id
floating_ip_id=$( openstack floating ip create --description bf_swarm_ip-$SERVER_PREFIX 730cb16e-a460-4a87-8c73-50a2cb2293f9 | grep " id" | awk '{ print $4 }' )

# get the loadbalancer vip port id
loadbalancer_port_id=$(openstack loadbalancer show bflb-$SERVER_PREFIX | grep vip_port_id | awk '{ print $4 }')

# associate the floating IP with the loadbalancer
openstack floating ip set --port $loadbalancer_port_id $floating_ip_id

# report back that we are done with the setup
floating_ip=$(openstack floating ip show $floating_ip_id | grep floating_ip_address | awk '{ print $4 }' )

# store the floating IP in /bf_config/floating_ip. The other two servers will use it to register them as part of the 
# bookface service, but with the floating IP as IP address

echo $floating_ip > /bf_config/floating_ip

log_message "Congratulations! Load balancer setup is complete. Floating IP of the service is: $floating_ip"

# this command adds the bookface service with the floating_ip into consul if consul is enabled
if [ -n "$ENABLE_CONSUL" ]; then 
JSON="{\"ID\": \"bookface.$COMPANY_NAME\", \"Name\": \"bookface\", \"Address\": \"$floating_ip\", \"Port\": 3000}"
curl -X PUT -d "$JSON" http://127.0.0.1:8500/v1/agent/service/register
fi

###################################
#### Migrate data from another bookface source
###################################

if [ ! -z "$MIGRATION_SOURCE" ]; then
log_message "Moving all the images into the /bf_images folder" 
# this command will take a while. We assume that the wget2 command from before is actually completed.
cp -r /$MIGRATION_SOURCE/images/* /bf_images
log_message "Last step of the import, time to add data to the DB. This will take a while. If autoscaling is enabled, it will be turned on once this step is completed."
# this command will take even longer. It uses a different import script, which skips the images. It may go a little faster than the previos import script.
curl -s "http://localhost/import_fast.php?entrypoint=$MIGRATION_SOURCE&key=$MIGRATION_KEY"
log_message "Data import complete"
fi

####
# Will we start the automatic scaling right away?
# If not, well print a helpful message on how to enable it later.
####

if [ -n "$ENABLE_AUTOSCALING" ]; then
log_message "Enabling autoscaling."
docker service update --env-add COMPANY_NAME="$COMPANY_NAME" --env-add BF_MAX_FRONTPAGE_USERS="$MAX_FRONTPAGE_USERS" bf_web
else
log_message "Autoscaling is possible but currently disabled, to enable it run this:"
log message "docker service update --env-add COMPANY_NAME=\"YOUR-COMPANY-NAME\" --env-add BF_MAX_FRONTPAGE_USERS=\"$MAX_FRONTPAGE_USERS\" bf_web"
fi


else
########################################
## THIS PART IS FOR WORKERS
##
## A "worker" is a server that does not end in "-1", like "server-2"
##
## Workers still end up as managers in the docker swarm. This term is only 
## used to organize the setup of the services
########################################

echo "I am a worker"
# this is stuff we can do while we wait

####
# GlusterFS
####

# starting a waiting loop until we are ready to conenct to everything else
log_message "Waiting for ${SERVER_PREFIX}-1 to set up GlusterFS. Quietly trying to mount /bf_config in the background every 5 seconds."
mount -t glusterfs ${HOSTNAME}:/bf_config /bf_config
sleep 3
while [ ! -f /bf_config/join-token ] ; do
sleep 10
echo "waiting to mount GlusterFS volume bf_config to be available"
umount /bf_config
mount -t glusterfs ${HOSTNAME}:/bf_config /bf_config
sleep 2
done

mount -t glusterfs ${HOSTNAME}:/bf_images /bf_images

log_message "GlusterFS folders mounted. Starting CockroachDB."

####
# CockroachDB
####

# Start the database
cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=$CDB_JOIN_STRING --advertise-addr=$(hostname):26257 --max-offset=1500ms
# add the command to the startup script
CDBCOMMAND="cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=$CDB_JOIN_STRING --advertise-addr=$(hostname):26257 --max-offset=1500ms"
echo cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=$CDB_JOIN_STRING --advertise-addr=$(hostname):26257 --max-offset=1500ms >> /root/bookface_start.sh

# Set up the special check script to monitor if the database needs to be started or not
echo "$CHECK_COCKROACHDB" | sed -e "s@CDBCOMMAND@$CDBCOMMAND@" > /root/check_cockroachdb.sh
chmod +x /root/check_cockroachdb.sh
echo "*/5 * * * * root /root/check_cockroachdb.sh" >> /etc/crontab

####
# Docker swarm
####

log_message "Waiting for Docker Swarm join-token to show up in /bf_config."
# Waiting for docker swarm token to show up
while ! [ -f /bf_config/join-token ]; do
echo "Waiting for join-token to show up"
sleep 3
done

# join the swarm
docker swarm join --token $( cat /bf_config/join-token ) ${SERVER_PREFIX}-1:2377

log_message "Swarm is joined. The rest is up to the manager to set up."

####
# Consul
####
if [ -n "$ENABLE_CONSUL" ]; then

log_message "Adding a consul agent to the datacenter"

# get the IP address
ip=$(cat /etc/hosts | grep $HOSTNAME | cut -f 1 -d " ")

# remove any dashes from the hostname, because DNS does not like it
DNShostname=$(echo $HOSTNAME | sed -e 's/-//g' )

# get the IP of server 1
server1ip=$(cat /etc/hosts | grep ${SERVER_PREFIX}-1 | cut -f 1 -d " ")

# create folders for consul to store its data outside of the container
mkdir /opt/consul
mkdir /opt/consul.conf

# start the consul container, acting as a local agent as well as a member of the local consul cluster
docker run --restart=always -d -v /opt/consul:/consul/data --name=consul --net=host -e 'CONSUL_LOCAL_CONFIG={"skip_leave_on_interrupt": true}' -e 'CONSUL_BIND_INTERFACE=ens3' consul agent -server -join=$server1ip --datacenter=$COMPANY_NAME -client=0.0.0.0 -node=$DNShostname

# wait for it to do it's thing
sleep 10

# start the consul_notifier, which automatically registers any services that export a public port
docker run -d --restart=always --name=consul_notifier -e CONSUL_ADDR=$ip --net=host --volume=/var/run/docker.sock:/var/run/docker.sock docker.cs.hioa.no/kyrrepublic/swarm-consul-notifier:latest

# we do not know the floating IP, so we wait for the first server to save it here before we move on
while [ ! -f /bf_config/floating_ip ]; do
sleep 5
done

# Let's also register this service 
floating_ip=$( cat /bf_config/floating_ip )
JSON="{\"ID\": \"bookface.$COMPANY_NAME\", \"Name\": \"bookface\", \"Address\": \"$floating_ip\", \"Port\": 3000}"
curl -X PUT -d "$JSON" http://127.0.0.1:8500/v1/agent/service/register
fi

fi
