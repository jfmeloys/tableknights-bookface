#!/bin/bash -x
if [[ $(sudo docker ps -a | grep memcached | awk '{ print $7 }') == "Exited" ]];
then
	sudo docker start memcache
	sudo docker ps -a | grep memcache
else
	echo "Memcache is running!"
fi
