#!/bin/bash -x

# Variables
VM=$1
IP=$2

source /home/ubuntu/DCSG2003_V21_group3.openrc.sh

# start maintenance VM
VM_ID=$( openstack server list | grep -w "maintenance" | awk '{print $2}')
openstack server start $VM_ID

# start apache2 to maintenance VM
VM_IP=$( openstack server list | grep -w "maintenance" | awk '{print $8}' | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}")
ssh ubuntu@$VM_IP "sudo service apache2 start; systemctl | grep apache2" 

# move floating IP from balancer to maintenance VM
BALANCER_IP=$(openstack server list | grep -w "balancer" | awk '{print $9}' | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}")
openstack server add floating ip maintenance $BALANCER_IP

sleep 2

curl http://$BALANCER_IP
